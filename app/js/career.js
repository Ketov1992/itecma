$('.vacancy__button').click(function () {
    jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: 0}, 350);
    $('.black-bg').fadeIn(400);
    setTimeout(function () {
        $('.career-popup').fadeIn(400);
    }, 480);
});

$('.career-popup__close').click(function () {
    $('.career-popup').fadeOut(400);
    setTimeout(function () {
        $('.black-bg').fadeOut(400);
    }, 480);
});