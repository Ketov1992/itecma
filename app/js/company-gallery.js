$(".fancybox").fancybox({minWidth:450});

$(".fancybox").fancybox({
    afterLoad: function() {
        this.title = '<div class="fancybox-text fancybox-text--company-gallery">' + this.element.data('text') + '</div>';
    },
    helpers : {
        title: {
            type: 'inside'
        }
    }
    
    /*afterLoad   : function() {
        this.inner.prepend( '<h1>1. My custom title</h1>' );
        this.content = '<h1>2. My custom title</h1>' + this.content.html();
    }*/
});

//Одинаковая высота колонок
$(function () {
    $('.team__employee').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
});
