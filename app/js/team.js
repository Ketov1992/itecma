$(".fancybox").fancybox({minWidth: 450});

$(".fancybox").fancybox({
    /*
    afterLoad: function () {
        this.title = '<div class="fancybox-h1">' + this.title + '</div>' + '<div class="fancybox-text">' + this.element.data('text') + '</div>';
    },
    helpers: {
        title: {
            type: 'inside'
        }
    }
    */

    /*
    afterLoad   : function() {
    this.inner.prepend( '<h1>1. My custom title</h1>' );
    this.content = '<h1>2. My custom title</h1>' + this.content.html();
    }
    */
   
    afterLoad: function () {
        this.title =    '<img src="' + this.element.data('bg') + '" alt="' + this.title + '" class="fancybox-image"/>' +
                        '<div class="fancybox-text"><div class="fancybox-h1">' + this.title + '</div><div class="fancybox-post">' + this.element.data('post') + '</div><div>' + this.element.data('text') + '</div></div>' +
                        '<div class="clear"></div>';
    },
    helpers: {
        title: {
            type: 'inside'
        }
    }
});

//Одинаковая высота колонок
$(function () {
    $('.team__employee').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
});