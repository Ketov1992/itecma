//Слайдер на главной
var sudoSlider = $(".slider").sudoSlider({
    slideCount: 1,
    continuous: true,
    speed: 300
});

$('.prevBtn').html('<div class="slider__arrow slider__arrow--left"><img src="images/slider-arrow-left.svg" alt="стрелка" class="slider__arrow-image slider__arrow-image--left-normal" /><img src="images/slider-arrow-left-active.svg" alt="стрелка" class="slider__arrow-image slider__arrow-image--left-active" /></div>');
$('.nextBtn').html('<div class="slider__arrow slider__arrow--right"><img src="images/slider-arrow-right.svg" alt="стрелка" class="slider__arrow-image slider__arrow-image--right-normal" /><img src="images/slider-arrow-right-active.svg" alt="стрелка" class="slider__arrow-image slider__arrow-image--right-active" /></div>');

//Фиксированное правое меню
$('.right-menu').simpleScrollFollow();


//Одинаковая высота колонок
$(function () {
    $('.main-block__column').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
});


//Работа с верхним меню
window.onscroll = function () {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled !== 0) {
        $('.header').addClass('header--active');
        $('.header__menu').addClass('header__menu--active');
    } else {
        $('.header').removeClass('header--active');
        $('.header__menu').removeClass('header__menu--active');
    }
};


//Прокрутка вверх
$('.footer__top').click(function () {
    jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: 0}, 350);
});


//Работа с селектом в попапе консультации
$('.consultation__input--select').click(function () {
    if ($(this).hasClass('consultation__input--select--active')) {
        $(this).removeClass('consultation__input--select--active');
        $(this).siblings('.consultation__select-variants').hide();
        $(this).siblings('.consultation__select-arrow').removeClass('consultation__select-arrow--active');
    } else {
        $(this).addClass('consultation__input--select--active');
        $(this).siblings('.consultation__select-variants').show();
        $(this).siblings('.consultation__select-arrow').addClass('consultation__select-arrow--active');
    }
});

$('.consultation__select-variants-li').click(function () {
    var el = $(this).parents('.consultation__select-variants').siblings('.consultation__input--select--active');
    var val = $(this).text();
    el.removeClass('consultation__input--select--active');
    el.siblings('.consultation__select-variants').hide();
    el.siblings('.consultation__select-arrow').removeClass('consultation__select-arrow--active');
    el.val(val);
});

$('.consultation__select-arrow').click(function () {
    var el = $(this).siblings('.consultation__input');
    if ($(this).hasClass('consultation__select-arrow--active')) {
        el.removeClass('consultation__input--select--active');
        el.siblings('.consultation__select-variants').hide();
        el.siblings('.consultation__select-arrow').removeClass('consultation__select-arrow--active');
    } else {
        el.addClass('consultation__input--select--active');
        el.siblings('.consultation__select-variants').show();
        el.siblings('.consultation__select-arrow').addClass('consultation__select-arrow--active');
    }
});


//Работа с попапом консультации
$('.header__phone').click(function () {
    jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: 0}, 350);
    $('.black-bg').fadeIn(400);
    setTimeout(function () {
        $('.consultation').fadeIn(400);
    }, 480);
});

$('.consultation__close').click(function () {
    $('.consultation').fadeOut(400);
    setTimeout(function () {
        $('.black-bg').fadeOut(400);
    }, 480);
});